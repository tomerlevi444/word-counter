let bulkUpdateIncrementalOp = (filter, op) => ({
	"updateOne": {
			filter,
			"update": { "$inc": op },
			"upsert": "true"
	}
})

const bulkWrite = (model, operations, options = {}) => {
  return new Promise((resolve, reject) => {
    model.collection.bulkWrite(operations, options, (error, response) => {
      if (error) {
        console.log(`Bulk write operation failed with error: ${error}`)
        reject(error)
      } else {
        console.log(`Bulk write operation completed successfully - updated ${response.upsertedCount + response.matchedCount} entities.`)
        resolve(response)
      }
    })
  })
}

export { bulkUpdateIncrementalOp, bulkWrite }
