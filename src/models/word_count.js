import mongoose from 'mongoose'
import { bulkUpdateIncrementalOp, bulkWrite } from '../db/helpers'
import { BULK_SIZE } from '../db/config'
import { chunkArray } from '../lib/helpers'

let WordCountSchema = new mongoose.Schema({
  word: { type: String, index: true },
  count: Number
})

let WordCount = mongoose.model('WordCount', WordCountSchema)

WordCount.bulkWrite = (word_count) => {
  let update_ops = Object.keys(word_count).map(word => bulkUpdateIncrementalOp({ word }, { count: word_count[word] }))

  chunkArray(update_ops, BULK_SIZE)
    .map(bulk_op => bulkWrite(WordCount, bulk_op)
      .catch(error => console.log(`Error while persisting update operation: ${error}`)))
}

export default WordCount
