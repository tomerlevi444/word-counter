let chunkArray = (array, chunk_size) => {
    let index = 0
    let arrayLength = array.length
    let result = []

    for (index = 0; index < arrayLength; index += chunk_size) {
        result.push(array.slice(index, index+chunk_size))
    }

    return result
}

export { chunkArray }
