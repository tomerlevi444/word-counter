let extractWords = text => (text.match(/\w+/g) || []).map(word => word.toLowerCase())

let countWords = text => {
  let words = extractWords(text)

	let mapping = {}
	for (let word of words) {
		mapping[word] = (mapping[word] || 0) + 1
	}

	return mapping
}

export { extractWords, countWords }
