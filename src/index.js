import http from 'http'
import express from 'express'
import morgan from 'morgan'
import connectToDB from './db/connect'
import api from './api'
import config from './config.json'

let app = express()
app.server = http.createServer(app)

// logger
app.use(morgan('dev'))

connectToDB()

// api router
app.use('/api', api)

app.server.listen(process.env.PORT || config.port, () => {
	console.log(`Started on port ${app.server.address().port}`)
})

export default app
