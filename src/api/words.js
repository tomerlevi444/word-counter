import express from 'express'
let router = express.Router()
import bodyParser from 'body-parser'
import fetch from 'node-fetch'

router.use(bodyParser.json())
import WordCountModel from '../models/word_count'
import { countWords, extractWords } from '../lib/text_manipulation'

// INSERTS/UPDATES WORDS FROM TEXT
router.put('/', async (req, res) => {
  if (!req.body.text && !req.body.endpoint) {
    res.status(400).send(`Request must include "text" or "endpoint" field`)
  }

  let text = req.body.text
  if (!text) {
    try {
      text = await(await fetch(req.body.endpoint)).text()
    } catch (error) {
      res.status(500).send(`There was a problem fetching endpoint ${req.body.endpoint}, error: ${error}`)
      return
    }
  }

  let word_count = countWords(text)

  res.status(200).send(word_count)

  WordCountModel.bulkWrite(word_count)
})

// GETS A SINGLE WORD FROM THE DATABASE
router.get('/:word', (req, res) => {
  const words = extractWords(req.params.word),
        word = words.length == 1 ? words[0] : null
  if (!word) {
    res.status(400).send("Provided parameter is not a well-structured word")
    return
  }

  WordCountModel.findOne({ word }, (error , entity) => {
      if (error) return res.status(500).send(`There was a problem finding the word, error: ${error}`)
      if (!entity) return res.status(404).send("Word not found.")
      res.status(200).send(entity.count.toString())
  })
})


// DELETES A WORD FROM THE DATABASE
router.delete('/:word', (req, res) => {
  const words = extractWords(req.params.word),
        word = words.length == 1 ? words[0] : null
  if (!word) {
    res.status(400).send("Provided parameter is not a well-structured word")
    return
  }

  WordCountModel.findOneAndRemove({ word }, (error, entity) => {
      if (error) return res.status(500).send(`There was a problem deleting the word, error: ${error}`)
      res.status(200).send(`Word: ${entity.word} was deleted.`)
  })
})

export default router
