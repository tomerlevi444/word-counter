import { version } from '../../package.json'
import { Router } from 'express'
import words from './words'

let api = Router()

api.use('/words', words)

api.get('/', (req, res) => {
	res.json({ version })
})

export default api
