Word Counter Service
==================================

Word counter service is written on top of Node.js using Express and MongoDB as a data warehouse.

Getting Started
---------------

# Install dependencies
`npm install`

# Start development live-reload server
`PORT=8080 npm run dev`

# Start development live-reload server with inspector
`PORT=8080 npm run dev -- --inspect`

# Start production server:
`PORT=8080 npm start`


Usage
-----

# Start server
`PORT=8080 npm run dev`

# Add words from raw text
`PUT http://localhost:8080/api/words`

CONTENT-TYPE: 'application/json'

BODY: { "text": <RAW_TEXT_GOES_HERE> }

# Add words from api endpoint
`PUT http://localhost:8080/api/words`

CONTENT-TYPE: 'application/json'

BODY: { "endpoint": <URL_GOES_HERE> }

# Get aggregated count for a specific word
`GET http://localhost:8080/api/words/<WORD_GOES_HERE>`


Data warehouse
--------------

This project uses [http://mlab.com](mLab) warehouse services to store the word-count data.
For admin rights, please contact tomlevpt@gmail.com.

License
-------

MIT
